(function ($, Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.entity_reference_views_search = {
    attach: function (context) {
      // jVectormap context.
      const ervsSelect = once('ervs-select', '.ervs-select', context);
      if (ervsSelect.length) {
        ervsSelect.forEach(processErvsSelection);
      }
    },
  };

  function processErvsSelection(element) {
    $(element).on('click', function(e) {
      const targetId = $(this).attr('data-id') || undefined;
      if (targetId) {
        handleClickSelection($(this), targetId);
        e.preventDefault();
      }
    });
  }

  function handleClickSelection($select, targetId) {
    const { rows, settings } = drupalSettings.entity_reference_views_search;

    // If selected is undefined, set it to null or handle the case appropriately
    const row = Object.values(rows).find((d) => d.entity_id === targetId) || undefined;

    if (row === undefined) {
      return;
    }

    const ajaxCallbackUrl = 'entity-reference-views-search/ajax';

    return new Promise(function(resolve, reject) {
      // Make an AJAX request to retrieve user data.
      $.ajax({
        url: Drupal.url(ajaxCallbackUrl),
        method: 'GET',
        data: {
          'row': JSON.stringify(row),
          'settings': JSON.stringify(settings),
        },
      })
      .then(function(data) {
        console.log(data.container);

        $('#' + data.container + ' .ervs-results > div').html(data.html);
        // Resolve with the user data object found or undefined.
        // resolve(data[0] || undefined);
      })
      .fail(function(xhr, status, error) {
        // Log an error message and reject the Promise on failure.
        // console.error(`User ${uid} request failed: ${error}`);
        console.error('Entity reference views search error: ' + error);
        reject(error);
      });
    });
  }

})(jQuery, Drupal, drupalSettings, once);
