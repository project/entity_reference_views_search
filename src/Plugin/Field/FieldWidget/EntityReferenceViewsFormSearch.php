<?php

namespace Drupal\entity_reference_views_search\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\entity_reference_views_search\Form\EntityFormViewsSearchSettingsForm;
use Drupal\field\FieldConfigInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity reference views search widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_views_search_default",
 *   label = @Translation("Entity form views search"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   },
 *   multiple_values = false
 * )
 */
class EntityReferenceViewsFormSearch extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity field manager.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Constructs an EntityReferenceViewsFormBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $keys = array_diff(parent::__sleep(), ['ervsHandler']);
    return $keys;
  }

  /**
   * The config settings name.
   *
   * @var string
   */
  const SETTINGS = 'entity_reference_views_search.settings';

  /**
   * The empty option label.
   *
   * @var string
   */
  const NONE_LABEL = '- None -';

  /**
   * {@inheritdoc}
   *
   * Explicitly handle the multiple values.
   */
  protected function handlesMultipleValues() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * Check if the field is applicable to use the widget.
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Get the handler settings.
    $handler_settings = $field_definition->getSetting('handler_settings');

    // Check if field definition is an instance of
    // \Drupal\field\FieldConfigInterface
    // and has target bundles.
    if ($field_definition instanceof FieldConfigInterface && isset($handler_settings['target_bundles'])) {
      // Multiple target bundles is not allowed to use the widget
      // and if disabled from the form field config settings.
      $is_applicable = count($handler_settings['target_bundles']) === 1 && $field_definition->getThirdPartySetting('entity_reference_views_search', 'status');

      // Return TRUE if applicable.
      // Otherwise, return FALSE.
      return $is_applicable;
    }

    // No option.
    return parent::isApplicable($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'searchable_fields' => NULL,
      'searchable_view' => NULL,
      'view_mode' => 'default',
      'autocomplete_off' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\field\FieldConfigInterface $field_definition */
    $field_definition = $this->fieldDefinition;

    // Get field settings.
    $handler_settings = $field_definition->getSetting('handler_settings');
    $target_bundle = reset($handler_settings['target_bundles']);
    $target_type = $field_definition->getSetting('target_type');

    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $entity_field_definitions */
    $entity_field_definitions = $this->entityFieldManager->getFieldDefinitions($target_type, $target_bundle);

    // Get allowed field types from the configuration.
    $allowed_field_types = $this->configFactory->get(self::SETTINGS)->get(EntityFormViewsSearchSettingsForm::SETTING_FIELD_TYPES);
    $allowed_field_types = Tags::explode($allowed_field_types);

    $element['searchable_view'] = [
      '#type' => 'select',
      '#title' => $this->t('Views result display'),
      '#options' => $this->getViewsOptions(),
      '#default_value' => $this->getSetting('searchable_view') ?? NULL,
      '#required' => TRUE,
    ];

    $element['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity display'),
      '#options' => $this->entityDisplayRepository->getViewModeOptions($target_type),
      '#default_value' => $this->getSetting('view_mode') ?? NULL,
      '#required' => TRUE,
    ];

    $element['autocomplete_off'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable input autocomplete'),
      '#default_value' => $this->getSetting('autocomplete_off'),
    ];

    // Build table header.
    $header = [
      'status' => NULL,
      'label' => $this->t('Label'),
      'name' => $this->t('Name'),
      'type' => $this->t('Type'),
      'arg' => $this->t('Filter identifier'),
      'placeholder' => $this->t('Placeholder'),
    ];

    // Build searchable fields element table.
    $element['searchable_fields'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('No applicable fields available.'),
      '#tree' => TRUE,
    ];

    // Get the searchable fields values.
    $searchable_fields = $this->getSetting('searchable_fields');

    // Get all the entity fields.
    foreach ($entity_field_definitions as $entity_field_definition) {
      $field_type = $entity_field_definition->getType();

      // Skip if field type is not allowed or
      // if field in not an instance of \Drupal\field\FieldConfigInterface.
      if (!$entity_field_definition instanceof FieldConfigInterface || !in_array($field_type, $allowed_field_types)) {
        continue;
      }

      // Get the information.
      $name = $entity_field_definition->getName();
      $label = $entity_field_definition->getLabel();
      $input_status_target = 'input[name="fields[' . $field_definition->getName() . '][settings_edit_form][settings][searchable_fields][' . $name . '][status]"]';

      // Build table status data.
      $element['searchable_fields'][$name]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Status'),
        '#title_display' => 'invisible',
        '#option' => $name,
        '#default_value' => $searchable_fields[$name]['status'] ?? NULL,
      ];

      // Build table label data.
      $element['searchable_fields'][$name]['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#title_display' => 'invisible',
        '#default_value' => $searchable_fields[$name]['label'] ?? $label,
        '#required' => TRUE,
      ];

      // Build table name data.
      $element['searchable_fields'][$name]['name'] = [
        '#type' => 'textfield',
        '#value' => $name,
        '#disabled' => TRUE,
      ];

      switch ($field_type) {
        case 'entity_reference':
          $element_type = 'autocomplete';
          break;

        default:
          $element_type = 'textfield';
          break;
      }

      // Build table type data.
      $element['searchable_fields'][$name]['type'] = [
        '#type' => 'textfield',
        '#value' => $element_type,
        '#disabled' => TRUE,
      ];

      // Build table argument data.
      $element['searchable_fields'][$name]['arg'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Argument'),
        '#title_display' => 'invisible',
        '#default_value' => $searchable_fields[$name]['arg'] ?? NULL,
        '#states' => [
          'disabled' => [
            $input_status_target => ['checked' => FALSE],
          ],
          'required' => [
            $input_status_target => ['checked' => TRUE],
          ],
        ],
      ];

      // Build table placeholder data.
      $element['searchable_fields'][$name]['placeholder'] = [
        '#type' => 'textfield',
        '#title_display' => 'invisible',
        '#default_value' => $searchable_fields[$name]['placeholder'] ?? NULL,
        '#states' => [
          'disabled' => [
            $input_status_target => ['checked' => FALSE],
          ],
        ],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    /** @var \Drupal\field\FieldConfigInterface $field_definition */
    $field_definition = $this->fieldDefinition;

    // Get the searchable fields values.
    $searchable_fields = $this->getSetting('searchable_fields') ?? [];
    $searchable_view = $this->getSetting('searchable_view') ?? NULL;

    $view_mode = $this->getSetting('view_mode') ?? NULL;
    $target_type = $field_definition->getSetting('target_type');
    $view_modes = $this->entityDisplayRepository->getViewModeOptions($target_type);

    // Get the number of fields in use.
    $count = count($this->getEnabledSearchableFields());

    // Set summary.
    $summary[] = $this->t('No. of fields in used: @count', ['@count' => $count]);
    $summary[] = $this->t('No. of applicable fields: @count', ['@count' => count($searchable_fields)]);
    $summary[] = $this->t('Views result display: @view', ['@view' => $searchable_view ? $this->getViewsOptions($searchable_view) : $this->t(self::NONE_LABEL)]);
    $summary[] = $this->t('Entity display: @view_mode', ['@view_mode' => $view_mode ? $view_modes[$view_mode] : $this->t(self::NONE_LABEL)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Get the searchable fields values.
    $enabled_fields = $this->getEnabledSearchableFields();
    $ajax_wrapper_id = $this->generateAjaxWrapper($items->getFieldDefinition()->id() . '_form');
    $target_id = !$items->isEmpty() ? $items->getValue()[0]['target_id'] : NULL;
    $autocomplete_off = (bool) $this->getSetting('autocomplete_off');

    // Build container element.
    $element = [
      '#type' => 'container',
      '#id' => str_replace('js-', '', $ajax_wrapper_id),
      '#attributes' => [
        'id' => 'ervs-container',
        'class' => ['ervs-container'],
        'data-ervs-ajax' => $ajax_wrapper_id,

        // Attribute for template usage.
        'data-ervs-type' => 'entity_reference_views_search',
      ],
      '#tree' => TRUE,
      '#weight' => -20,
    ];

    // Build element for target ID.
    $element['target_id'] = [
      '#type' => 'hidden',
      '#default_value' => $target_id,
      '#attributes' => [
        'class' => ['ervs-input'],
      ],
    ];

    // Build element for each enabled fields.
    foreach ($enabled_fields as $field_name => $field) {
      $element[$field_name] = [
        '#type' => $field['type'],
        '#title' => $field['label'],
      ];

      // Add a placeholder attribute.
      if ($field['placeholder']) {
        $element[$field_name]['#placeholder'] = $field['placeholder'];
      }

      // Disable autocomplete for inputs.
      if ($autocomplete_off) {
        $element[$field_name]['#attributes']['autocomplete'] = 'none';
      }
    }

    // Build element for entity view.
    $element['preview'] = [
      '#type' => 'container',
      '#id' => $ajax_wrapper_id . '-ervs-preview',
      '#weight' => 9,
      '#access' => $target_id,
    ];
    $element['preview']['output'] = [
      '#markup' => $this->entityViewRenderer($target_id),
    ];

    // Build element for search results.
    $element['results'] = [
      '#type' => 'container',
      '#prefix' => '<div id="' . $ajax_wrapper_id . '" class="ervs-results">',
      '#suffix' => '</div>',
      '#weight' => 10,
      '#tree' => TRUE,
    ];

    // Build element for actions.
    $element['actions'] = [
      '#type' => 'actions',
      '#weight' => -10,
    ];
    $element['actions']['button'] = [
      '#type' => 'button',
      '#value' => $this->t('Search for existing records'),
      '#ajax' => [
        'wrapper' => $ajax_wrapper_id,
        'callback' => [$this, 'entityReferenceViewsSearchAjax'],
      ],
    ];

    return $element;
  }

  /**
   * Entity reference views search AJAX callback function.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function entityReferenceViewsSearchAjax(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\field\FieldConfigInterface $field_definition */
    $field_definition = $this->fieldDefinition;

    // Get information.
    $field_name = $field_definition->getName();
    $ajax_wrapper_id = $form[$field_name]['widget']['#attributes']['data-ervs-ajax'];
    $enabled_fields = $this->getEnabledSearchableFields();
    $exposed_filters = [];

    // Create an AJAX response.
    $response = new AjaxResponse();

    // Get all the values.
    $values = $form_state->getValue([
      $field_name,
    ]);

    // Loop through enabled fields to map the values for exposed filter format.
    foreach ($enabled_fields as $field_name => $enabled_field) {
      $arg = $enabled_field['arg'];
      $exposed_filters[$arg] = $values[$field_name] ?? NULL;
    }

    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->viewsViewRenderer(FALSE);

    // Set exposed filter values.
    $view->setExposedInput($exposed_filters);

    // Run attachments.
    $view->preExecute();

    // Execute views query.
    $view->execute();

    // Invoke JS command to set attributes in the container.
    $response->addCommand(new InvokeCommand('.ervs-container[data-ervs-ajax="' . $ajax_wrapper_id . '"]', 'attr', [
      [
        'data-ervs-view-id' => $view->id(),
        'data-ervs-view-display' => $view->current_display,
      ],
    ]));

    // Replace the content of results with the rendered views.
    $response->addCommand(new ReplaceCommand('#' . $ajax_wrapper_id . ' > div', $view->render()));

    // Remove preview.
    $response->addCommand(new ReplaceCommand('#' . $ajax_wrapper_id . '-ervs-preview', NULL));

    return $response;
  }

  /**
   * {@inheritdoc}
   *
   * Produce the proper target ID format.
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if ($values['target_id']) {
      return $values['target_id'];
    }
  }

  /**
   * Generates AJAX wrapper by field ID.
   *
   * @param string $field_id
   *   The field ID to be appended to the wrapper.
   *
   * @return string
   *   Returns the generated AJAX wrapper ID.
   */
  protected function generateAjaxWrapper(string $field_id) : string {
    $ajax_wrapper_id = 'js-entity_reference_views_search_';
    $ajax_wrapper_id .= str_replace('.', '_', $field_id);
    return Html::getId($ajax_wrapper_id);
  }

  /**
   * Get the enabled searchable fields.
   *
   * @return array
   *   An array of enabled searchable fields.
   */
  protected function getEnabledSearchableFields() : array {
    $searchable_fields = $this->getSetting('searchable_fields');
    $enabled_fields = [];
    if (!$searchable_fields) {
      return $enabled_fields;
    }
    foreach ($searchable_fields as $field_name => $field) {
      if ($field['status'] == 1) {
        $enabled_fields[$field_name] = $field;
      }
    }
    return $enabled_fields;
  }

  /**
   * Get the views list.
   *
   * @param string $option
   *   The option key from the options.
   *
   * @return mixed
   *   An array of views options or the individual view option.
   */
  protected function getViewsOptions($option = NULL) {
    $views_options = Views::getViewsAsOptions();
    if (isset($views_options[$option])) {
      return $views_options[$option];
    }
    return $views_options;
  }

  /**
   * Entity view renderer.
   *
   * @param mixed $target_id
   *   The entity target ID.
   *
   * @return mixed
   *   Returns the entity view root.
   */
  protected function entityViewRenderer($target_id) {
    if (!$target_id) {
      return;
    }

    // Get the referenced entity target type.
    $target_type = $this->fieldDefinition->getSetting('target_type');

    /** @var \Drupal\Core\Entity\EntityInterface $referenced_entity */
    $referenced_entity = $this->entityTypeManager->getStorage($target_type)->load($target_id);

    if ($referenced_entity instanceof EntityInterface) {
      $view_builder = $this->entityTypeManager->getViewBuilder($target_type);
      $build = $view_builder->view($referenced_entity, $this->getSetting('view_mode'));
      return $this->renderer->render($build);
    }
  }

  /**
   * Views renderer.
   *
   * @param bool $return_renderable
   *   Identifier for returning the views output or the views object.
   * @param array $arguments
   *   An array of views arguments for filters.
   *
   * @return mixed
   *   An array of views options or the individual view option.
   */
  protected function viewsViewRenderer(bool $return_renderable = TRUE, array $arguments = []) {
    $view = $this->getSetting('searchable_view');
    $view_parts = explode(':', $this->getSetting('searchable_view'));
    $view_id = $view_parts[0] ?? NULL;
    $display_id = $view_parts[1] ?? NULL;

    /** @var \Drupal\views\ViewExecutable $view */
    $view = Views::getView($view_id);

    // Set views display.
    $view->setDisplay($display_id);

    // Check if views exists.
    if (!$view instanceof ViewExecutable || !$view->storage->getDisplay($display_id)) {
      return;
    }

    // Set views arguments.
    if ($arguments) {
      $view->setArguments($arguments);
    }

    // Get renderable views.
    if ($return_renderable) {
      $view->preExecute();
      $view->execute();
      return $view->buildRenderable();
    }

    return $view;
  }

}
